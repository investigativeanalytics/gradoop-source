## Gradoop: Distributed Graph Analytics on Hadoop

All the material contained in this directory was taken verbatim from the
[Gradoop framework](http://www.gradoop.com) source code repository.

Copyright, 2014 - 2017 Leipzig University (Database Research Group)

Such material is distributed under the Apache [License version
2.0](https://www.apache.org/licenses/LICENSE-2.0), available in the LICENSE
file in this directory.
