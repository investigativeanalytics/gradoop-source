output_dir=/tmp/shark-sna

if [ -d "$output_dir" ]; then
    echo ""
    echo "Output directory $output_dir exists. Please, remove it before proceeding."
    echo ""
    exit 1
fi

# Ugly hack to run  the program: see note at bottom.   Get the classpaths from
# maven and isolate the one of this module (it's the second).
java -cp ./gradoop-source-examples/target/classes:$(mvn dependency:build-classpath|sed -n '/Dependencies classpath/{n;p;}'|sed '/^$/d'|tail -1) sh.serene.shark.examples.SNABenchmark1 ./gradoop-source-examples/target/classes/sna $output_dir

# Alternate version, just merges classpaths of both modules:
# java -cp ./gradoop-source-examples/target/classes:$(mvn dependency:build-classpath|sed -n '/Dependencies classpath/{n;p;}'|sed '/^$/d'|sed 'N;s/\n/:/') sh.serene.shark.examples.SNABenchmark1 ./gradoop-source-examples/target/classes/sna $output_dir

echo ""
echo "The results are saved in $output_dir. Remove this directory before rerunning."
echo ""

# NOTE:
#
# Somehow the program doesn't run correctly when invoce from maven through the
# exec plugin. The command should be the following:
#
# mvn exec:java -X -e -pl gradoop-source-examples -Dexec.classpathScope=test -Dexec.mainClass="sh.serene.shark.examples.SNABenchmark1" -Dexec.args="./gradoop-source-examples/target/classes/sna /tmp/shark-sna"
#
# The current  implementation is a  hack to keep  things going, but  the issue
# should be investigated.
