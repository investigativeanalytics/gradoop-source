## Gradoop data-source experiments

This project collects experiments concerned with feeding extended property
graph model (EPGM) data to the [Gradoop framework](http://www.gradoop.org/),
in the form of JSON messages delivered thorugh a
[Kafka](http://kafka.apache.org/) stream.

## Functioning

The code builds on a social network analysis (SNA) example included in Gradoop
(writte in Java), which performs the following task:

1. Read an  EPGM graph  from  JSON-lines files  (graphs.json, nodes.json  and
   edges.json)
2. Extract a subgraph with:
  - vertex predicate: must be of type 'Person'
  - edge predicate: must be of type 'knows'
3. Group the subgraph using the vertex attributes 'city' and 'gender' and
  - count the number of vertices represented by each super vertex
  - count the number of edges represented by each super edge
4. Aggregate the grouped graph:
  - add the total vertex count as new graph property
  - add the total edge count as new graph property

The result is stored in EPGM JSON-lines format in a directory.

For  the  time being  this  repository  only  contains the  original  example,
extracted from the  Gradoop codebase, and what's necessary to  make it compile
and run independently.

Additionally  the  repository contains  an  independent  Scala program,  which
implements  a Kafka  consumer capable  of  receiving EPGM  data in  JSON-lines
format. Currently the consumer prints the messages it receives. Eventually the
consumer will become  an EPGM data source, that receives  message from a Kafka
stream and builds the corresponding Gradoop graph.

## Repository structure

The repository is divided in 2 modules:

* *gradoop-source-adapters*: contains the Scala-based Kafka EPGM consumer.
* *gradoop-source-examples*: contains  the Java-based social  network analysis
   example.

## Compiling and running

Compiling and running the  applications at the moment is a bit  of a hack. For
compilation *maven*  is required.  For running  the examples, at the  moment a
Bash shell is required (that is, to run them comfortably).

To build the Java sources (SNA application):

```bash
mvn compile
```

To build the Scala sources (Kafka consumer):

```bash
mvn scala:compile
```

To run the  test applications two helper script are  provided. The SNA example
is self  contained, with input files  are provided in the  repository. Results
will  be writte  to /tmp.  To run  the SNA  example it's  enough to  issue the
following command from the repository root:

```bash
./run_sna.sh
```

The Kafka consumer example requires  a (minimal) Kafka installation. Run Kafka
and create a topic called epgm_topic:

```bash
bin/kafka-server-start.sh config/server.properties
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 10 --topic epgm_topic
```

Start the Kafka EPGM consumer, which will wait listening to the epgm_topic:

```bash
./run_source.sh
```

Finally, from another terminal window, feed EPGM data into the Kafka stream, using e.g. the example EPGM files:

```bash
kafka-console-producer.sh --broker-list localhost:9092 --topic epgm_topic < ./gradoop-source-examples/src/main/resources/sna/edges.json
kafka-console-producer.sh --broker-list localhost:9092 --topic epgm_topic < ./gradoop-source-examples/src/main/resources/sna/graphs.json
kafka-console-producer.sh --broker-list localhost:9092 --topic epgm_topic < ./gradoop-source-examples/src/main/resources/sna/vertices.json
```
